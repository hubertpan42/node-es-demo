let fetch = require('node-fetch')

console.log('start');

let queryBody = {
    "query":{
         "bool":{
              "must":{"match": {"item_dsc" : "juice"}},
              "must_not":{"match":{"item_dsc" : "orange"}}
         }
    }
}

console.log('make post call')
//create an http post call to the target url using JSON as the post body
let requestPromise = fetch('http://localhost:9200', {method:'post', body: JSON.stringify(queryBody), headers:{'Content-Type':'application/json'}});

//if the request failed to connect to the server log the error
requestPromise.catch(function(error){
    console.log("Error request failed:\n");
    console.log(error);
});


//if the request sucessfully completed extract the response body as json otherwise log the error
let requestBodyAsJsonPromise = requestPromise.then(function(res){
    if(res.ok){
        return res.json();
    } else {
        throw new Error(`Connection completed but error was thrown by server: ${res.status}: ${res.statusText}`)
    }
})

//if the response was returned with a 200 "OK" status and the json body was successfully extracted, log it out.
requestBodyAsJsonPromise.then(function(json){
    console.log(json);
})

//if the response with returned with a non 200 "OK" status or if the json body was not successfully extracted, log the error
requestBodyAsJsonPromise.then(function(error){
    console.log(error);
});

console.log("fin")